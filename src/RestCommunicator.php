<?php
declare(strict_types=1);
namespace RestFrontendApi;
use Exception;
use Netpromotion\Profiler\Profiler;
use Nette\Application\AbortException;
use Nette\Application\UI\Presenter;
use Nette\Security\User;

/**
 * Class RestCommunicator
 * The base class for the all APIs
 * @author Martin Urbanczyk
 */
class RestCommunicator {

    /** @var Logger $logger - logger service */
    private $logger;

    /** @var null $accessToken */
    private $accessToken;

    /** @var string $apiUrl */
    private $apiUrl;

    /** @var int $apiTimeout */
    private $apiTimeout;

    /** @var Presenter */
    private $presenter;

    /** @var int */
    private $env;

    /** @var string */
    private $link;

    /** @var User */
    private $user;

    /**
     * RestCommunicator constructor.
     * @param string $apiUrl
     * @param int $apiTimeout
     * @param int $env
     * @param Logger $logger
     */
    public function __construct(string $apiUrl, int $apiTimeout, int $env, Logger $logger) {

        $this->logger = $logger;
        $this->apiUrl = $apiUrl;
        $this->apiTimeout = $apiTimeout;
        $this->env = $env;
    }

    /** @param string $accessToken */
    public function setAccessToken(string $accessToken): void {
        $this->accessToken = $accessToken;
    }

    /** @param User $user */
    public function setUser(User $user): void {
        $this->user = $user;
    }

    /**
     * @param Presenter $presenter
     * @param string $link
     */
    public function setPresenterWithRedirect(Presenter $presenter, string $link): void {
        $this->presenter = $presenter;
        $this->link = $link;
    }

    /**
     * @param string $url
     * @param IBaseEntity|null $object
     * @param string $type
     * @return bool|mixed|string
     * @throws ErrorApiException
     * @throws WarningApiException
     * @throws AbortException
     */
    public function sendRequestObject(string $url, ? IBaseEntity $object, string $type) {

        if ($this->env === 0) Profiler::start("$type/$url");

        $ch = curl_init($this->apiUrl . $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,$this->apiTimeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->apiTimeout);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $encodedArray = null;
        if ($object !== null && ($type === 'POST' || $type === 'PUT')) {

            // Find collection
            foreach (get_object_vars($object) as $nameOfProperty => $valueOfProperty) {
                if (strpos($nameOfProperty, 'Collection') !== FALSE) {
                    $encodedArray = json_encode($object->$nameOfProperty, JSON_THROW_ON_ERROR);
                }
            }

            // We don't have collection
            if ($encodedArray === null) {
                $encodedObject = json_encode($object, JSON_THROW_ON_ERROR);
                $arrayObject = json_decode($encodedObject, true, 512, JSON_THROW_ON_ERROR);
                $filteredArray = $this->filterNotNull($arrayObject);
                $encodedArray = json_encode($filteredArray, JSON_THROW_ON_ERROR);
            }

            // Set header
            curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedArray);
            if($this->accessToken === null){
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'Accept-Language: cs',
                    'Content-Length: ' . strlen($encodedArray)]);
            } else {
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json', 'Accept-Language: cs',
                    'Authorization: Bearer ' . $this->accessToken,
                    'Content-Length: ' . strlen($encodedArray)]
                );
            }
        }
        else if ($this->accessToken === null) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Accept-Language: cs']);
        } else {
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Accept-Language: cs',
                'Authorization: Bearer ' . $this->accessToken]);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Logout on unauthorized
        if ($code === 403 && $this->presenter !== null && $this->user !== null) {
            $this->user->logout();
            $this->presenter->redirect($this->link);
        }

        if ($code === 500 || $result === false || $result === true) {
            $this->logger->generateCurlLog($code, func_get_args(), $this->apiUrl, $this->accessToken, json_encode($result), false, $encodedArray);
            if ($this->env === 0) Profiler::finish("$type/$url");
            throw new ErrorApiException($code, func_get_args(), $this->apiUrl, $this->accessToken);
        }

        // Decode
        $originalResult = $result;
        if ($originalResult === '') $result = null;
        else $result = json_decode($result, true);

        // If we have some error
        if ($code >= 300) {
            $this->logger->generateCurlLog($code, func_get_args(), $this->apiUrl, $this->accessToken, $originalResult, false, $encodedArray);
            if ($this->env === 0) Profiler::finish("$type/$url");
            throw new WarningApiException($code, func_get_args(), $this->apiUrl, $this->accessToken, $originalResult);
        }

        // Return the result
        $this->logger->generateCurlLog($code, func_get_args(), $this->apiUrl, $this->accessToken, $originalResult, true, $encodedArray);
        if ($this->env === 0) Profiler::finish("$type/$url");
        return $result;
    }

    /**
     * @param array $array
     * @return array
     */
    public function filterNotNull(array $array): array {

        $array = array_map(function($item) {return is_array($item) ? $this->filterNotNull($item) : $item;}, $array);
        return array_filter($array, static function($item) {
            return $item !== '' && $item !== null && (!is_array($item) || count($item) > 0);
        });
    }
}

/**
 * Class ErrorApiException
 * @package RestFrontendApi
 */
class ErrorApiException extends Exception {

    /** @var array */
    public $callParameters;

    /** @var string  */
    public $endPoint;

    /** @var string|null  */
    public $token;

    /**
     * ErrorApiException constructor.
     * @param int $httpCode
     * @param array $callParameters
     * @param string $endPoint
     * @param string|null $token
     * @param Exception|null $prev
     * @param string $exceptionTitle
     */
    public function __construct(int $httpCode, array $callParameters, string $endPoint, ? string $token,
                                Exception $prev = null, string $exceptionTitle = 'API Internal Error!') {

        parent::__construct($exceptionTitle, $httpCode, $prev);
        $this->callParameters = $callParameters;
        $this->endPoint = $endPoint;
        $this->token = $token;
    }
}

/**
 * Class WarningApiException
 * @package RestFrontendApi
 */
class WarningApiException extends ErrorApiException {

    /** @var string  */
    public $curlResult;

    /**
     * WarningApiException constructor.
     * @param int $httpCode
     * @param array $callParameters
     * @param string $endPoint
     * @param string|null $token
     * @param string $curlResult
     * @param Exception|null $prev
     */
    public function __construct(int $httpCode, array $callParameters, string $endPoint, ? string $token, string $curlResult, Exception $prev = null) {
        parent::__construct($httpCode, $callParameters, $endPoint, $token, $prev, 'API Warning Error!');
        $this->curlResult = $curlResult;
    }
}