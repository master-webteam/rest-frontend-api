<?php

namespace RestFrontendApi;
use DateTime;
use Nette\Utils\FileSystem;

/**
 * Class Logger
 * Logger service for the APIs
 * @author Martin Pavelka
 * @package App\Models\System
 */
class Logger {

    /** Location of is api log */
    const LogPath = __DIR__ . '/../../../../log/';

    /** The string with the line */
    const Line = "----------------------------------------------------------------------------------------------------";

    /** @var string $fileName - the name of the log file */
    private $fileName;

    /** @var string|null $logPathWithDir - the full path to log file */
    private $logDirName;

    /** @var Slack */
    private $slack;

    /** @var int $env - environment 0 for devel */
    private $env;

    /** @var string $projectName - name of the project */
    private $projectName;

    /**
     * Logger constructor.
     * @param string $logDirName
     * @param int $env - environment 0 for develop
     * @param string $projectName - just some name for project
     * @param Slack $slack - slack service
     */
    public function __construct(string $logDirName, int $env, string $projectName, Slack $slack) {

        $this->logDirName = $logDirName;
        $this->slack = $slack;
        $this->env = $env;
        $this->projectName = $projectName;
    }

    /**
     * Generate log entry for the APIs
     * @param string $space - namespace for caller
     * @param string $func - name of the function
     * @param array|null $params - parameters array
     * @param array|null $data - data from API
     * @param bool $success - if success or failure (1 is success)
     * @param bool $cached - if the output is cached value
     * @param array $parentParams - another params from the base class f.e.
     * @throws \Nette\IOException
     */
    public function generateOutputLog(string $space, string $func, ?array $params, ?array $data, bool $success,
                                      bool $cached, array $parentParams) : void {

        // Set paths
        if ($this->logDirName === null) $this->logDirName = 'Other';
        $logPathWithDir = self::LogPath . $this->logDirName . '/';

        // If no log dir create one
        if (!file_exists($logPathWithDir) && !is_dir($logPathWithDir))
            FileSystem::createDir($logPathWithDir, 0777);

        // Get output file name
        if ($success) $this->generateLogFileNameSuccess();
        else $this->generateLogFileNameError();

        // Env
        if ($this->env === 0) $env = "ENV: Development";
        else if ($this->env === 1) $env = "ENV: Testing";
        else if ($this->env === 2) $env = "ENV: Production";
        else $env = "ENV: None";

        // Get data
        $who = "WHO: " . $space . "-" . $func . " (" . ($cached ? 'CACHED' : 'NON-CACHED') . ")";
        $path = "PATH: " . __DIR__;
        $parameters = "PARAMETERS: " . json_encode($params, JSON_UNESCAPED_SLASHES);
        $result = "DATA: " . json_encode($data, JSON_UNESCAPED_SLASHES);
        $obj = "BASE: " . json_encode($parentParams, JSON_UNESCAPED_SLASHES);
        $outputData = $env . "\n" . $path . "\n" . $who . "\n" . $parameters . "\n" . $result . "\n" . $obj . "\n" .
            self::Line . "\n";$outputData;
        file_put_contents($logPathWithDir . $this->fileName, $outputData, FILE_APPEND);
        if (!$success) $this->slack->sendMessage($outputData);

    }

    /** Generate the file name for the success */
    private function generateLogFileNameSuccess() {

        $now = new DateTime();
        $this->fileName = $now->format('Y-m-d') . '-OK.txt';
    }

    /** Generate the file name for the failure */
    private function generateLogFileNameError() {

        $now = new DateTime();
        $this->fileName = $now->format('Y-m-d') . '-ERR.txt';
    }

    /**
     * Generate the curl log
     * @param int $httpCode - http code
     * @param array $callParameters - calling parameters
     * @param string $endPoint - server
     * @param string $token - token
     * @param null|string $curlResult - result
     * @param bool $success - success
     * @param string $inputData
     */
    public function generateCurlLog(int $httpCode, array $callParameters, string $endPoint, ? string $token,
                                    ?string $curlResult, bool $success, ? string $inputData = null) : void {

        // Set paths
        if ($this->logDirName === null) $this->logDirName = 'Other';
        $logPathWithDir = self::LogPath . $this->logDirName . '/';

        // If no log dir create one
        if (!file_exists($logPathWithDir) && !is_dir($logPathWithDir))
            FileSystem::createDir($logPathWithDir);

        // Get output file name
        if ($success) $this->generateLogFileNameSuccess();
        else $this->generateLogFileNameError();

        // Env
        if ($this->env === 0) $env = "ENV: Development";
        else if ($this->env === 1) $env = "ENV: Testing";
        else if ($this->env === 2) $env = "ENV: Production";
        else $env = "ENV: None";

        // Get data
        $who = "WHO: " . $endPoint;
        $token = "TOKEN: " . $token;
        $parameters = "PARAMETERS: " . json_encode($callParameters, JSON_UNESCAPED_SLASHES);
        $input = "INPUT:" . $inputData;
        $result = "DATA: " . json_encode($curlResult, JSON_UNESCAPED_SLASHES);
        $http = "HTTP: " . $httpCode;
        $outputData = $env . "\n" . $token . "\n" . $who . "\n" . $parameters . "\n" . $input . "\n" . $result .
            "\n" . $http . "\n" . self::Line . "\n";
        file_put_contents($logPathWithDir . $this->fileName, $outputData, FILE_APPEND);
        if (!$success) $this->slack->sendMessage($outputData);
    }

    /**
     * Generate the payload log
     * @param int $code
     * @param int $user
     * @param array $payload
     */
    public function generatePayloadLog(int $code, int $user, array $payload) : void {

        // Env
        if ($this->env === 0) $env = "ENV: Development";
        else if ($this->env === 1) $env = "ENV: Testing";
        else if ($this->env === 2) $env = "ENV: Production";
        else $env = "ENV: None";

        // Get data
        $system = $this->projectName;
        $type = "[PAYLOAD ERROR]";
        $codeText = "CODE: " . $code;
        $userText = "USER: " . $user;
        $payloadText = "PAYLOAD: " . json_encode($payload, JSON_UNESCAPED_SLASHES);
        $outputData = $system . "\n" . $env . "\n" . $type . "\n" . $codeText . "\n" . $userText . "\n"
            . $payloadText . "\n" . self::Line . "\n";
        $this->slack->sendMessage($outputData);
    }
}
