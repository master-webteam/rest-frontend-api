<?php
namespace RestFrontendApi;

/**
 * Class Slack
 * @author Martin Urbanczyk
 * @package App\Models\System
 */
class Slack {

    private $slackHook;

    /**
     * Slack constructor.
     * @param string $slackHook
     */
    public function __construct(string $slackHook) {
        $this->slackHook = $slackHook;
    }

    /**
     * Send message using the web hook
     * @param string $message
     */
    public function sendMessage(string $message) : void {

        $ch = curl_init($this->slackHook);
        $data = json_encode(["text" => $message]);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_exec($ch);
        curl_close($ch);
    }
}
