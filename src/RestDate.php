<?php

namespace RestFrontendApi;
use DateTime;

/**
 * Class RestDate
 * @package RestFrontendApi
 */
class RestDate extends GraphQLDates {

    /**
     * Convert datetime into custom graphql date
     * @param $instance - instance of datetime
     * @return RestDate
     */
    static function convertToRestDate(DateTime $instance) : GraphQLDate {
        $converted =  new RestDate();
        $converted->setTimestamp($instance->getTimestamp());
        return $converted;
    }
}