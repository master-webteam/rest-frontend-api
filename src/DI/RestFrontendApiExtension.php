<?php

namespace RestFrontendApi\DI;
use Nette\DI\CompilerExtension;
use RestFrontendApi\Slack;
use RestFrontendApi\Logger;
use RestFrontendApi\RestCommunicator;

class RestFrontendApiExtension extends CompilerExtension {

    public function loadConfiguration() : void  {

        $config = $this->getConfig();
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('slack'))
            ->setClass(Slack::class, [$config['slackHook']]);

        $builder->addDefinition($this->prefix('logger'))
            ->setClass(Logger::class, [$config['loggerDir'], $config['env'], $config['project']]);

        $builder->addDefinition($this->prefix('communication'))
            ->setClass(RestCommunicator::class, [$config['apiUrl'], $config['apiTimeout'], $config['env']]);
    }
}
