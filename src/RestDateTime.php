<?php

namespace RestFrontendApi;
use DateTime;

/**
 * Class RestDateTime
 * @package RestFrontendApi
 */
class RestDateTime extends GraphQLDates {

    /**
     * Convert datetime into custom graphql date
     * @param $instance - instance of datetime
     * @return RestDateTime
     */
    static function convertToRestDate(DateTime $instance) : GraphQLDateTime {
        $converted =  new RestDateTime();
        $converted->setTimestamp($instance->getTimestamp());
        return $converted;
    }
}