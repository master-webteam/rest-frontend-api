<?php

namespace RestFrontendApi;
use PhpDocReader\AnnotationException;
use PhpDocReader\PhpDocReader;
use ReflectionException;
use ReflectionProperty;

/**
 * Class BaseRestEntity
 * @package RestFrontendApi
 */
abstract class BaseRestEntity extends BaseRest {

    /**
     * Construct object from the result
     * @param array $inputArrayFromJson - input array with json decoded data from API
     * @throws EntityNotFoundInTheResultArray - entity declared name not found in the result
     * @throws ReflectionException - error with the reflexion function
     * @throws AnnotationException - error with the reader lib
     * @todo when we have parameter type list call construct collection instead of array (f.e. tickets messages)
     */
    public function constructFromInputArray(array $inputArrayFromJson): void {

        foreach ($this->getEntityParameters() as $parameter) {


            // We have date in two formats omg (fuck it all https://youtu.be/Px-FRo1K3DE)
            $reader = new PhpDocReader();
            $property = new ReflectionProperty($this, $parameter);
            $propertyClass = $reader->getPropertyClass($property);

            // RestDateTime
            if ($propertyClass === "RestFrontendApi\RestDateTime" && isset($inputArrayFromJson[$parameter])) {
                $dateValueString = $inputArrayFromJson[$parameter];
                $dateTimeObject = RestDateTime::createFromFormat('Y-m-d\TH:i:s', $dateValueString);
                $dateTimeObjectAlter = RestDateTime::createFromFormat('Y-m-d\TH:i', $dateValueString);
                $this->$parameter = ($dateTimeObject === false) ? RestDateTime::convertToRestDate($dateTimeObjectAlter)
                    : RestDateTime::convertToRestDate($dateTimeObject);
                if ($this->$parameter === false) $this->$parameter = null;
                continue;
            }

            // RestDateTime
            if ($propertyClass === "RestFrontApi\RestDate" && isset($inputArrayFromJson[$this->usedNameInResult][$parameter])) {
                $dateValueString = $inputArrayFromJson[$this->usedNameInResult][$parameter];
                $dateTimeObject = RestDate::convertToRestDate(RestDate::createFromFormat('Y-m-d',
                    $dateValueString), RestDate::class);
                $this->$parameter = $dateTimeObject;
                if ($this->$parameter === false) $this->$parameter = null;
                continue;
            }

            // Another
            $this->$parameter = isset($inputArrayFromJson[$parameter]) ? $inputArrayFromJson[$parameter] : null;
        }
    }

    /**
     * Init the collection from the input array from API
     * @param array $inputCollectionArrayFromJson - input array from API result
     * @param string $classToInit - class name in the collection, with namespace
     * @throws AnnotationException - problem with the reader lib
     * @throws EntityNotFoundInTheResultArray - not found in the result exception
     * @throws ReflectionException - problem with the reflection function
     * @throws WrongCollectionParametersException - wrong collection entity on the input
     */
    public function constructCollectionFromInputArray(array $inputCollectionArrayFromJson, string $classToInit): void {

        $collectionParameters = $this->getEntityParameters();
        if (count($collectionParameters) !== 1) throw new WrongCollectionParametersException();
        $collectionParameter = current($collectionParameters);
        $this->$collectionParameter = $this->$collectionParameter ?? [];
        foreach ($inputCollectionArrayFromJson as $item) {
            $createdObject = new $classToInit();
            $createdObject->constructFromInputArray($item);
            array_push($this->$collectionParameter, $createdObject);
        }
    }

    /**
     * Get just raw parameters
     * @noinspection PhpUndefinedClassInspection
     * @return array - array of selecting ones
     */
    protected function getEntityParameters() : array {
        return array_keys(get_object_vars($this));
    }
}